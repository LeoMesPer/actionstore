﻿namespace ActionStore.Telas.Pedido
{
    partial class ConsultarPD
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dgvpedido = new System.Windows.Forms.DataGridView();
            this.label7 = new System.Windows.Forms.Label();
            this.txtpedido = new System.Windows.Forms.TextBox();
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.ID_Pedido = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.fk_Cliente = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.fk_Produto = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.vl_qtd_pedido = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dt_pedido = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.vl_total = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.dgvpedido)).BeginInit();
            this.SuspendLayout();
            // 
            // dgvpedido
            // 
            this.dgvpedido.AllowUserToAddRows = false;
            this.dgvpedido.AllowUserToDeleteRows = false;
            this.dgvpedido.ColumnHeadersHeight = 40;
            this.dgvpedido.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.ID_Pedido,
            this.fk_Cliente,
            this.fk_Produto,
            this.vl_qtd_pedido,
            this.dt_pedido,
            this.vl_total});
            this.dgvpedido.Location = new System.Drawing.Point(3, 53);
            this.dgvpedido.Name = "dgvpedido";
            this.dgvpedido.RowHeadersVisible = false;
            this.dgvpedido.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.dgvpedido.Size = new System.Drawing.Size(577, 288);
            this.dgvpedido.TabIndex = 56;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.BackColor = System.Drawing.Color.Transparent;
            this.label7.Font = new System.Drawing.Font("Segoe UI", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.Color.White;
            this.label7.Location = new System.Drawing.Point(12, 8);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(102, 32);
            this.label7.TabIndex = 57;
            this.label7.Text = "Pedido:";
            // 
            // txtpedido
            // 
            this.txtpedido.Location = new System.Drawing.Point(119, 12);
            this.txtpedido.Multiline = true;
            this.txtpedido.Name = "txtpedido";
            this.txtpedido.Size = new System.Drawing.Size(306, 28);
            this.txtpedido.TabIndex = 58;
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.Color.Black;
            this.button1.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button1.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button1.ForeColor = System.Drawing.Color.Orange;
            this.button1.Location = new System.Drawing.Point(449, 8);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(107, 35);
            this.button1.TabIndex = 59;
            this.button1.Text = "BUSCAR";
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // button2
            // 
            this.button2.BackColor = System.Drawing.Color.Black;
            this.button2.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.button2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button2.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button2.ForeColor = System.Drawing.Color.Orange;
            this.button2.Location = new System.Drawing.Point(3, 300);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(89, 41);
            this.button2.TabIndex = 60;
            this.button2.Text = "Menu";
            this.button2.UseVisualStyleBackColor = false;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // ID_Pedido
            // 
            this.ID_Pedido.DataPropertyName = "ID_Pedido";
            this.ID_Pedido.HeaderText = "ID";
            this.ID_Pedido.Name = "ID_Pedido";
            // 
            // fk_Cliente
            // 
            this.fk_Cliente.DataPropertyName = "fk_Cliente";
            this.fk_Cliente.HeaderText = "Cliente";
            this.fk_Cliente.Name = "fk_Cliente";
            // 
            // fk_Produto
            // 
            this.fk_Produto.DataPropertyName = "fk_Produto";
            this.fk_Produto.HeaderText = "Produto";
            this.fk_Produto.Name = "fk_Produto";
            // 
            // vl_qtd_pedido
            // 
            this.vl_qtd_pedido.DataPropertyName = "vl_qtd_pedido";
            this.vl_qtd_pedido.HeaderText = "Quantidade";
            this.vl_qtd_pedido.Name = "vl_qtd_pedido";
            // 
            // dt_pedido
            // 
            this.dt_pedido.DataPropertyName = "dt_pedido";
            this.dt_pedido.HeaderText = "Data";
            this.dt_pedido.Name = "dt_pedido";
            // 
            // vl_total
            // 
            this.vl_total.DataPropertyName = "vl_total";
            this.vl_total.HeaderText = "Total";
            this.vl_total.Name = "vl_total";
            // 
            // ConsultarPD
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Black;
            this.ClientSize = new System.Drawing.Size(579, 339);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.txtpedido);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.dgvpedido);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "ConsultarPD";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "ConsultarPD";
            ((System.ComponentModel.ISupportInitialize)(this.dgvpedido)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView dgvpedido;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox txtpedido;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.DataGridViewTextBoxColumn ID_Pedido;
        private System.Windows.Forms.DataGridViewTextBoxColumn fk_Cliente;
        private System.Windows.Forms.DataGridViewTextBoxColumn fk_Produto;
        private System.Windows.Forms.DataGridViewTextBoxColumn vl_qtd_pedido;
        private System.Windows.Forms.DataGridViewTextBoxColumn dt_pedido;
        private System.Windows.Forms.DataGridViewTextBoxColumn vl_total;
    }
}
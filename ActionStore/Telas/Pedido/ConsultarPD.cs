﻿using ActionStore.Programação.PedidoP;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ActionStore.Telas.Pedido
{
    public partial class ConsultarPD : Form
    {
        public ConsultarPD()
        {
            InitializeComponent();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            {
                frmMenu frm = new frmMenu();
                frm.Show();
                Hide();
            }
        }
        //va
        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                PedidoBusiness business = new PedidoBusiness();
                List<PedidoDTO> pedido = business.Consultar();

                dgvpedido.AutoGenerateColumns = false;
                dgvpedido.DataSource = pedido;
            }
            catch (ArgumentException ex)
            {
                MessageBox.Show(ex.Message, "AS",
                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Ocorreu um erro, tente mais tarde." + ex.Message, "AS",
                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
    }
}

﻿using ActionStore.Programação.ClienteP;
using ActionStore.Programação.PedidoP;
using ActionStore.Programação.ProdutoP;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ActionStore.Telas.Pedido
{
    public partial class frmCadastrarPD : Form
    {
        public frmCadastrarPD()
        {
            InitializeComponent();
            CarregarGrid();
            CarregarProdutos();
            CarregarCliente();
        }

        private void label2_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            {
                frmMenu frm = new frmMenu();
                frm.Show();
                Hide();
            }
        }

        private void button2_Click_1(object sender, EventArgs e)
        {
            {
                frmMenu frm = new frmMenu();
                frm.Show();
                Hide();
            }
        }
        private void CarregarProdutos()
        {
            ProdutoBusiness business = new ProdutoBusiness();
            List<ProdutoDTO> produtos = business.Consultar();

            cboproduto.ValueMember = nameof(ProdutoDTO.ID_Produto);
            cboproduto.DisplayMember = nameof(ProdutoDTO.nm_Produto);
            cboproduto.DataSource = produtos;
        }
        private void CarregarCliente()
        {
            ClienteBusiness business = new ClienteBusiness();
            List<ClienteDTO> cliente = business.Consultar();

            cbocliente.ValueMember = nameof(ClienteDTO.ID_Cliente);
            cbocliente.DisplayMember = nameof(ClienteDTO.nm_cliente);
            cbocliente.DataSource = cliente;
        }

        private void btnemitir_Click(object sender, EventArgs e)
        {
            try
            {
                //ID_Pedido, fk_Estoque, fk_Produto, fk_Cliente, vl_qtd_pedido
                PedidoDTO dto = new PedidoDTO();
                ProdutoDTO produto = new ProdutoDTO();
                dto.fk_Produto = Convert.ToInt32(cboproduto.SelectedValue);
                //dto.fk_Estoque = Convert.ToInt32(cboproduto.SelectedValue);
                dto.fk_Cliente = Convert.ToInt32(cbocliente.SelectedValue);
                dto.vl_qtd_pedido = Convert.ToInt32(txtqtd.Text);
                dto.dt_pedido = DateTime.Now;
                
                dto.vl_total = Convert.ToDecimal(txttotal.Text);

                     
                PedidoBusiness business = new PedidoBusiness();
                business.Salvar(dto);


                MessageBox.Show("Compra efetuada com sucesso!", "AS", MessageBoxButtons.OK, MessageBoxIcon.Information);
              
            }
            catch (ArgumentException ex)
            {
                MessageBox.Show(ex.Message, "AS", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "AS", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }
        BindingList<PedidoDTO> produtosCarrinho = new BindingList<PedidoDTO>();

        private void CarregarGrid()
        {
            dgvproduto.AutoGenerateColumns = false;
            dgvproduto.DataSource = produtosCarrinho;
        }

        private void btnadd_Click(object sender, EventArgs e)
        {
            try
            {
                ProdutoDTO dto = cboproduto.SelectedItem as ProdutoDTO;
                PedidoDTO item = new PedidoDTO();

                item.vl_qtd_pedido = Convert.ToInt32(txtqtd.Text);
                item.fk_Produto = dto.ID_Produto;


                for (int i = 0; i < item.vl_qtd_pedido; i++)
                {
                    produtosCarrinho.Add(item);
                }

                decimal quantidade = Convert.ToDecimal(txtqtd.Text);
                decimal valor = Convert.ToDecimal(dto.vl_preco);

                SomarValor(quantidade, valor);
            }
            catch (ArgumentException ex)
            {
                MessageBox.Show(ex.Message, "AS", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "AS", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        decimal ValorTotal = 0;
        private void SomarValor(decimal quantidade, decimal valor)
        {
            decimal valorTotal = quantidade * valor;
            ValorTotal = ValorTotal + valorTotal;
            txttotal.Text = ValorTotal.ToString();
        }

        private void btnmais_Click(object sender, EventArgs e)
        {

            {
                {
                    frmCadastrar frm = new frmCadastrar();
                    frm.Show();
                    Hide();
                }
            }
        }

        private void cboproduto_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
    }
}

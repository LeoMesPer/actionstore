﻿using ActionStore.Programação.FornecedorP;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ActionStore.Telas.Fornecedor
{
    public partial class frmCadastrarF : Form
    {
        public frmCadastrarF()
        {
            InitializeComponent();
        }

        private void label2_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            {
                frmMenu frm = new frmMenu();
                frm.Show();
                Hide();
                Close();
            }
        }

        private void button2_Click_1(object sender, EventArgs e)
        {
            {
                frmMenu frm = new frmMenu();
                frm.Show();
                Hide();
               
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                FornecedorDTO dto = new FornecedorDTO();
                dto.nm_fornecedor = txtnome.Text.Trim();
                dto.ds_email = txtemail.Text.Trim();
                dto.dt_entrega = Convert.ToDateTime(dtdt.Text.Trim());
                dto.ds_cnpj = Convert.ToInt32(txtcnpj.Text.Trim());
                dto.ds_cep = txtcep.Text.Trim();
                dto.ds_tel_cel = txttel.Text.Trim();
                dto.ds_complemento = txtcomp.Text.Trim();
                dto.ds_numero_empresa = Convert.ToInt32(txtn.Text.Trim());
                dto.vl_preco = Convert.ToDecimal(txtpreco.Text.Trim());

                FornecedorBusiness business = new FornecedorBusiness();
                business.Salvar(dto);
                MessageBox.Show("Fornecedor Salvo com sucesso.", "AS",
                    MessageBoxButtons.OK, MessageBoxIcon.Information);
                {
                    frmMenu frm = new frmMenu();
                    frm.Show();
                    Hide();
                }


            }
            catch (ArgumentException ex)
            {
                MessageBox.Show(ex.Message, "AS",
                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Ocorreu um erro, tente mais tarde." + ex.Message, "AS",
                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
    }
}

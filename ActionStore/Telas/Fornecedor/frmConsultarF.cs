﻿using ActionStore.Programação.FornecedorP;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ActionStore.Telas.Fornecedor
{
    public partial class frmConsultarF : Form
    {
        public frmConsultarF()
        {
            InitializeComponent();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            {
                frmMenu frm = new frmMenu();
                frm.Show();
                Hide();
             
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                FornecedorBusiness business = new FornecedorBusiness();
                List<FornecedorDTO> fornecedor = new List<FornecedorDTO>();

                if (txtfornecedor.Text != String.Empty)
                {
                    //fornecedor = business.ConsultarPorCnpj((txtfornecedor.Text.Trim()));
                }
                else
                {
                    fornecedor = business.Consultar();
                }

                dgvfornecedor.AutoGenerateColumns = false;
                dgvfornecedor.DataSource = fornecedor;
            }
            catch (ArgumentException ex)
            {
                MessageBox.Show(ex.Message, "AS",
                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Ocorreu um erro, tente mais tarde." + ex.Message, "AS",
                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
    }
}

﻿using ActionStore.Programação.ProdutoP;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ActionStore.Produto
{
    public partial class frmConsultarPT : Form
    {
        public frmConsultarPT()
        {
            InitializeComponent();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            {
                frmMenu frm = new frmMenu();
                frm.Show();
                Hide();
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                ProdutoBusiness business = new ProdutoBusiness();
                List<ProdutoDTO> fornecedor = new List<ProdutoDTO>();

                if (txtproduto.Text != String.Empty)
                {
                    //fornecedor = business.ConsultarPorCnpj((txtfornecedor.Text.Trim()));
                }
                else
                {
                    fornecedor = business.Consultar();
                }

                dataGridView1.AutoGenerateColumns = false;
                dataGridView1.DataSource = fornecedor;
            }
            catch (ArgumentException ex)
            {
                MessageBox.Show(ex.Message, "AS",
                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Ocorreu um erro, tente mais tarde." + ex.Message, "AS",
                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
    }
}

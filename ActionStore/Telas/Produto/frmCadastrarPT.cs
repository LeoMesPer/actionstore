﻿using ActionStore.Programação.FornecedorP;
using ActionStore.Programação.ProdutoP;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ActionStore.Produto
{
    public partial class frmCadastrarPT : Form
    {
        public frmCadastrarPT()
        {
            InitializeComponent();
            CarregarFornecedores();
        }

        private void label2_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            {
                frmMenu frm = new frmMenu();
                frm.Show();
                Hide();
            }
        }

        private void button2_Click_1(object sender, EventArgs e)
        {
            {
                frmMenu frm = new frmMenu();
                frm.Show();
                Hide();

            }
        }
        private void CarregarFornecedores()
        {
            FornecedorBusiness business = new FornecedorBusiness();
            List<FornecedorDTO> fornecedores = business.Consultar();

            cbfornecedor.ValueMember = nameof(FornecedorDTO.ID_Fornecedor);
            cbfornecedor.DisplayMember = nameof(FornecedorDTO.nm_fornecedor);
            cbfornecedor.DataSource = fornecedores;
        }
        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                ProdutoDTO dto = new ProdutoDTO();
                dto.nm_Produto = txtnome.Text;
                dto.vl_preco = Convert.ToDecimal(txtpreco.Text);
                dto.ds_descricao = txtdescricao.Text;
                dto.fk_Fornecedor = Convert.ToInt32(cbfornecedor.SelectedValue);

                ProdutoDatabase business = new ProdutoDatabase();
                business.Salvar(dto);

                MessageBox.Show("Produto cadastrado com sucesso!", "AS", MessageBoxButtons.OK, MessageBoxIcon.Information);
                {
                    frmMenu frm = new frmMenu();
                    frm.Show();
                    Hide();
                }
                //Close();
            }
            catch (ArgumentException ex)
            {
                MessageBox.Show(ex.Message, "AS", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "AS", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
    }
}

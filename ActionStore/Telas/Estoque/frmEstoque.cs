﻿using ActionStore.Programação.EstoqueP;
using ActionStore.Programação.ProdutoP;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ActionStore.Telas.Estoque
{
    public partial class frmEstoque : Form
    {
        public frmEstoque()
        {
            InitializeComponent();
            CarregarProdutos();
            CarregarGrid();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            {
                frmMenu frm = new frmMenu();
                frm.Show();
                Hide();
            }
        }
        private void CarregarGrid()
        {
            try
            {
                EstoqueBusiness business = new EstoqueBusiness();
                List<EstoqueDTO> fornecedor = new List<EstoqueDTO>();

                fornecedor = business.Consultar();

                dgvqtd.AutoGenerateColumns = false;
                dgvqtd.DataSource = fornecedor;
            }
            catch (ArgumentException ex)
            {
                MessageBox.Show(ex.Message, "AS", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "AS", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        private void CarregarProdutos()
        {
            ProdutoBusiness business = new ProdutoBusiness();
            List<ProdutoDTO> prod = business.Consultar();

            cboproduto.DisplayMember = nameof(ProdutoDTO.nm_Produto);
            cboproduto.ValueMember = nameof(ProdutoDTO.ID_Produto);
            cboproduto.DataSource = prod;
        }
        private void btnAdd_Click(object sender, EventArgs e)
        {
            try
            {

                EstoqueDTO dto = new EstoqueDTO();
                dto.qtd_produto = Convert.ToInt32(txtqtd.Text);
                dto.fk_Produto = Convert.ToInt32(cboproduto.SelectedValue);

                EstoqueBusiness business = new EstoqueBusiness();
                business.Salvar(dto);
                MessageBox.Show("Estoque atualizado com sucesso!", "AS", MessageBoxButtons.OK, MessageBoxIcon.Information);
                CarregarGrid();
            }
            catch (ArgumentException ex)
            {
                MessageBox.Show(ex.Message, "AS", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "AS", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
           
        }
    }
}

﻿using ActionStore.Cliente;
using ActionStore.Programação.ClienteP;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ActionStore
{
    public partial class frmCadastrar : Form
    {
        public frmCadastrar()
        {
            InitializeComponent();
        }

        private void label2_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void label15_Click(object sender, EventArgs e)
        {
            {
                frmConsultarC frm = new frmConsultarC();
                frm.Show();
                Hide();
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            {
                frmMenu frm = new frmMenu();
                frm.Show();
                Hide();
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            {
                frmMenu frm = new frmMenu();
                frm.Show();
                Hide();
            }
        }

        private void textBox10_TextChanged(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                ClienteDTO dto = new ClienteDTO();
                dto.nm_cliente = txtnome.Text.Trim();
                dto.ds_Email = txtemail.Text.Trim();
                dto.tp_pessoa = cmdtdp.Text.Trim();
                dto.dt_nascimento = Convert.ToDateTime(dtdt.Text.Trim());
                dto.ds_cpf_cnpj = txtcpf.Text.Trim();
                dto.ds_rg = txtrg.Text.Trim();
                dto.ds_cep = txtcep.Text.Trim();
                dto.ds_tel_cel = Convert.ToInt32(txtcel.Text.Trim());
                dto.ds_complemento = txtcomp.Text.Trim();
                dto.ds_numero_casa = Convert.ToInt32(txtn.Text.Trim());

                ClienteBusiness business = new ClienteBusiness();
                business.Salvar(dto);
                MessageBox.Show("Cliente Salvo com sucesso.", "AS",
                    MessageBoxButtons.OK, MessageBoxIcon.Information);
                {
                    frmMenu frm = new frmMenu();
                    frm.Show();
                    Hide();
                }


            }
            catch (ArgumentException ex)
            {
                MessageBox.Show(ex.Message, "AS",
                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Ocorreu um erro, tente mais tarde." + ex.Message, "AS",
                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        private void txtn_TextChanged(object sender, EventArgs e)
        {

        }
    }
    }


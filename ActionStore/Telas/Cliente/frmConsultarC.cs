﻿using ActionStore.Programação.ClienteP;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ActionStore.Cliente
{
    public partial class frmConsultarC : Form
    {
        public frmConsultarC()
        {
            InitializeComponent();
            CarregarGrid();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            {
                frmMenu frm = new frmMenu();
                frm.Show();
                Hide();
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                ClienteBusiness business = new ClienteBusiness();
                List<ClienteDTO> clientes = new List<ClienteDTO>();

                if (txtcliente.Text != string.Empty)
                {
                    clientes = business.ConsultarPorNome(txtcliente.Text.Trim());
                }
                else
                {
                    clientes = business.Consultar();
                }

                dgvcliente.AutoGenerateColumns = false;
                dgvcliente.DataSource = clientes;
            }
            catch (ArgumentException ex)
            {
                MessageBox.Show(ex.Message, "AS",
                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Ocorreu um erro, tente mais tarde." + ex.Message, "AS",
                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        private void dgvcliente_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }
        private void CarregarGrid()
        {
            try
            {
                ClienteBusiness business = new ClienteBusiness();
                List<ClienteDTO> fornecedor = new List<ClienteDTO>();

                fornecedor = business.Consultar();

                dgvcliente.AutoGenerateColumns = false;
                dgvcliente.DataSource = fornecedor;
            }
            catch (ArgumentException ex)
            {
                MessageBox.Show(ex.Message, "AS", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "AS", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btn_Remover_Click(object sender, EventArgs e)
        {
            //try
            //{
            //    ClienteDTO data = dgvcliente.CurrentRow.DataBoundItem as ClienteDTO;

            //    ClienteBusiness business = new ClienteBusiness();
            //    business.RemoverCliente(data.ID_Cliente);

            //    MessageBox.Show("Cliente Removido com Sucesso!", "Black Fit LTDA", MessageBoxButtons.OK, MessageBoxIcon.Information);
            //    CarregarGrid();
            //}
            //catch (ArgumentException ex)
            //{
            //    MessageBox.Show(ex.Message, "ActionStore", MessageBoxButtons.OK, MessageBoxIcon.Error);
            //}
            //catch (Exception)
            //{
            //    MessageBox.Show("Ocorreu um erro não identificado.", "ActionStore", MessageBoxButtons.OK, MessageBoxIcon.Error);
            //}
        }

        //private void dgvcliente_CellContentClick(object sender, DataGridViewCellEventArgs e)
        //{
        //    try
        //    {
        //        if (e.ColumnIndex == 6)
        //        {
        //            ClienteDTO Cliente = dgvcliente.Rows[e.RowIndex].DataBoundItem as ClienteDTO;

        //            DialogResult r = MessageBox.Show($"Deseja realmente excluir o pedido {Cliente.ID_Cliente}?", "AS",
        //                                MessageBoxButtons.YesNo, MessageBoxIcon.Question);

        //            if (r == DialogResult.Yes)
        //            {
        //                ClienteDTO cat = dgvcliente.CurrentRow.DataBoundItem as ClienteDTO;

        //                ClienteBusiness business = new ClienteBusiness();
        //                business.RemoverCliente(cat.ID_Cliente);

        //                button1_Click(null, null);
        //            }
        //        }
        //    }
        //    catch (ArgumentException ex)
        //    {
        //        MessageBox.Show(ex.Message, "AS",
        //            MessageBoxButtons.OK, MessageBoxIcon.Error);
        //    }
        //    catch (Exception ex)
        //    {
        //        MessageBox.Show("Ocorreru um erro, tente mais tarde.", "AS",
        //            MessageBoxButtons.OK, MessageBoxIcon.Error);
        //    }
        //}
    }
    }


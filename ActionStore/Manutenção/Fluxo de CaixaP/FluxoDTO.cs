﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ActionStore.Programação.Fluxo_de_CaixaP
{
    public class FluxoDTO
    {
        //ID_Fluxo, vl_ganho, vl_gasto, fk_Pedido, fk_Fornecedor
        public int ID_Fluxo { get; set; }
        public decimal vl_ganho { get; set; }
        public decimal vl_gasto { get; set; }
        public int fk_Pedido { get; set; }
        public decimal fk_Fornecedor { get; set; }
        }
}

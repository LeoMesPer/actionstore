﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ActionStore.Programação.ProdutoP
{
    class ProdutoBusiness
    {
        public int Salvar(ProdutoDTO dto)
        {
            if (dto.nm_Produto == string.Empty)
            {
                throw new ArgumentException("Nome é obrigatório.");
            }

            //ProdutoDTO cat = this.ConsultarPorNome(dto.nm_Produto);
            //if (cat != null)
            //{
            //    throw new ArgumentException("Categoria já existe no sistema.");
            //}

            ProdutoDatabase db = new ProdutoDatabase();
            return db.Salvar(dto);
        }


        public void Remover(int id)
        {
            ProdutoDatabase db = new ProdutoDatabase();
            db.Remover(id);
        }

        public List<ProdutoDTO> Consultar()
        {
            ProdutoDatabase db = new ProdutoDatabase();
            return db.Consultar();
        }


        public List<ProdutoDTO> Listar()
        {
            ProdutoDatabase db = new ProdutoDatabase();
            return db.Listar();
        }

    }
}

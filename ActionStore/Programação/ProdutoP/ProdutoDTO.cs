﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ActionStore.Programação.ProdutoP
{
   public class ProdutoDTO
    {
        //ID_Produto, nm_Produto, vl_preco, ds_descricao, fk_Fornecedor
        public int ID_Produto { get; set; }
        public string nm_Produto { get; set; }
        public decimal vl_preco { get; set; }
        public string ds_descricao { get; set; }
        public int fk_Fornecedor { get; set; }
    }
}

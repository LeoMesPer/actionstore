﻿using ActionStore.Base;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ActionStore.Programação.ProdutoP
{
    class ProdutoDatabase
    {
        public int Salvar(ProdutoDTO dto)
        {
            string script = @"INSERT INTO tb_Produto (ID_Produto, nm_Produto, vl_preco, ds_descricao, fk_Fornecedor) 
                                   VALUES (@ID_Produto, @nm_Produto, @vl_preco, @ds_descricao, @fk_Fornecedor)";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("ID_Produto", dto.ID_Produto));
            parms.Add(new MySqlParameter("nm_Produto", dto.nm_Produto));
            parms.Add(new MySqlParameter("vl_preco", dto.vl_preco));
            parms.Add(new MySqlParameter("ds_descricao", dto.ds_descricao));
            parms.Add(new MySqlParameter("fk_Fornecedor", dto.fk_Fornecedor));

            Database db = new Database();
            return db.ExecuteInsertScriptWithPk(script, parms);
        }

        public void Remover(int id)
        {
            string script = @"DELETE FROM tb_Produto WHERE ID_Produto = @ID_Produto";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("ID_Produto", id));

            Database db = new Database();
            db.ExecuteInsertScript(script, parms);
        }


        public List<ProdutoDTO> Listar()
        {
            string script = @"SELECT * FROM tb_Produto";

            List<MySqlParameter> parms = new List<MySqlParameter>();

            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            List<ProdutoDTO> lista = new List<ProdutoDTO>();
            while (reader.Read())
            {
                ProdutoDTO dto = new ProdutoDTO();
                dto.ID_Produto = reader.GetInt32("ID_Produto");
                dto.nm_Produto = reader.GetString("nm_Produto");
                dto.fk_Fornecedor = reader.GetInt32("fk_Fornecedor");
                dto.ds_descricao = reader.GetString("ds_descricao");
                dto.vl_preco = reader.GetDecimal("vl_preco");

                lista.Add(dto);
            }
            reader.Close();

            return lista;
        }


        public List<ProdutoDTO> Consultar()
        {
            string script = @"SELECT * FROM tb_Produto";

            List<MySqlParameter> parms = new List<MySqlParameter>();

            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            List<ProdutoDTO> lista = new List<ProdutoDTO>();
            while (reader.Read())
            {
                ProdutoDTO dto = new ProdutoDTO();
                dto.ID_Produto = reader.GetInt32("ID_Produto");
                dto.nm_Produto = reader.GetString("nm_Produto");
                dto.fk_Fornecedor = reader.GetInt32("fk_Fornecedor");
                dto.ds_descricao = reader.GetString("ds_descricao");
                dto.vl_preco = reader.GetDecimal("vl_preco");

                lista.Add(dto);
            }
            reader.Close();

            return lista;
        }
    }
}

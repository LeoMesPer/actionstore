﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ActionStore.Programação.ClienteP
{
    class ClienteBusiness
    {
        public int Salvar(ClienteDTO dto)
        {
            if (dto.ds_Email == string.Empty)
            {
                throw new ArgumentException("Email é obrigatório.");
            }
            if (dto.ds_cep == string.Empty)
            {
                throw new ArgumentException("Cep é obrigatório.");
            }
            
            if (dto.nm_cliente == string.Empty)
            {
                throw new ArgumentException("Nome é obrigatório.");
            }
            if (dto.ds_cpf_cnpj == string.Empty)
            {
                throw new ArgumentException("CPF/Cnpj é obrigatório.");
            }
            if (dto.ds_rg == string.Empty)
            {
                throw new ArgumentException("RG é obrigatório.");
            }
            if (dto.dt_nascimento == null)
            {
                throw new ArgumentException("Data é obrigatório.");
            }
            if (dto.tp_pessoa == string.Empty)
            {
                throw new ArgumentException("Tipo de Pessoa é obrigatório.");
            }
            if (dto.ds_cep == string.Empty)
            {
                throw new ArgumentException("Cep é obrigatório.");
            }
            if (dto.ds_numero_casa == 0)
            {
                throw new ArgumentException("N° casa é obrigatório.");
            }
           
            ClienteDTO cat = this.ConsultarPorCpf(dto.ds_cpf_cnpj);
            if (cat != null)
            {
                throw new ArgumentException("CPF já existe no sistema.");
            }
         
            ClienteDatabase db = new ClienteDatabase();
            return db.Salvar(dto);
        }


        //public int AlterarCliente(ClienteDTO dto)
        //{
            //if (dto.ds_Bairro == string.Empty)
            //    throw new ArgumentException("Bairro não pode estar vazio.");

            //if (dto.ds_Logradouro == string.Empty)
            //    throw new ArgumentException("Logradouro não pode estar vazio.");

            //if (dto.ds_UF == string.Empty)
            //    throw new ArgumentException("UF não pode estar vazio.");

            //if (dto.ds_CEP == string.Empty)
            //    throw new ArgumentException("CEP não pode estar vazio.");

            //ValidarNumero regexNum = new ValidarNumero();
            //ValidarTexto regexTxt = new ValidarTexto();
            //regexTxt.ValidarEmail(dto.ds_Email);
            //regexTxt.ValidarNome(dto.nm_Nome);
            //regexNum.ValidarTelefoneFixo(dto.num_Telefone);
            //regexNum.ValidarTelefoneCelular(dto.num_Celular);

            //CPF validar = new CPF();
            //validar.ValidarCPF(dto.ds_CPF);

            //DESCripto cripto = new DESCripto();
            //dto.nm_Nome = cripto.Criptografar(Program.chave, dto.nm_Nome);
            //dto.ds_CPF = cripto.Criptografar(Program.chave, dto.ds_CPF);
            //dto.num_Celular = cripto.Criptografar(Program.chave, dto.num_Celular);
            //dto.num_Telefone = cripto.Criptografar(Program.chave, dto.num_Telefone);
            //dto.ds_Email = cripto.Criptografar(Program.chave, dto.ds_Email);

            //ClientesDatabase db = new ClientesDatabase();
            //return db.AlterarCliente(dto);
        //}

        public int RemoverCliente(int idCliente)
        {
            ClienteDatabase db = new ClienteDatabase();
            return db.RemoverCliente(idCliente);
        }


        public List<ClienteDTO> ConsultarPorNome(string nome)
        {
            ClienteDatabase db = new ClienteDatabase();
            return db.ConsultarPorNome(nome);
        }


        public ClienteDTO ConsultarPorCpf(string cpf)
        {
            ClienteDatabase db = new ClienteDatabase();
            return db.ConsultarPorCpf(cpf);
        }

        public List<ClienteDTO> Consultar()
        {
            ClienteDatabase db = new ClienteDatabase();
            return db.Consultar();
        }

        public List<ClienteDTO> Listar()
        {
            ClienteDatabase db = new ClienteDatabase();
            return db.Listar();
        }

    }
}

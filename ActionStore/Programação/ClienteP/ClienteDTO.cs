﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ActionStore.Programação.ClienteP
{
    public class ClienteDTO
    {
        //ID_Cliente, ds_Email, nm_cliente, tp_pessoa, dt_nascimento, ds_tel_cel, ds_cpf_cnpj, ds_rg, ds_cep, ds_numero_casa, ds_complemento
        public int ID_Cliente { get; set; }
        public string ds_Email { get; set; }
        public string nm_cliente { get; set; }
        public string tp_pessoa { get; set; }
        public DateTime dt_nascimento { get; set; }
        public int ds_tel_cel { get; set; }
        public string ds_cpf_cnpj { get; set; }
        public string ds_rg { get; set; }
        public string ds_cep { get; set; }
        public int ds_numero_casa { get; set; }
        public string ds_complemento { get; set; }
    }
}

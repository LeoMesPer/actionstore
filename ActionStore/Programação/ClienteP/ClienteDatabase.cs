﻿using ActionStore.Base;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ActionStore.Programação.ClienteP
{
    class ClienteDatabase
    {
        public int Salvar(ClienteDTO dto)
        {
            string script = @"INSERT INTO tb_Cliente (ID_Cliente, ds_Email, nm_cliente, tp_pessoa, dt_nascimento, ds_tel_cel, ds_cpf_cnpj, ds_rg, ds_cep, ds_numero_casa, ds_complemento) 
                                   VALUES (@ID_Cliente, @ds_Email, @nm_cliente, @tp_pessoa, @dt_nascimento, @ds_tel_cel, @ds_cpf_cnpj, @ds_rg, @ds_cep, @ds_numero_casa, @ds_complemento)";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("ID_Cliente", dto.ID_Cliente));
            parms.Add(new MySqlParameter("ds_Email", dto.ds_Email));
            parms.Add(new MySqlParameter("nm_cliente", dto.nm_cliente));
            parms.Add(new MySqlParameter("tp_pessoa", dto.tp_pessoa));
            parms.Add(new MySqlParameter("dt_nascimento", dto.dt_nascimento));
            parms.Add(new MySqlParameter("ds_tel_cel", dto.ds_tel_cel));
            parms.Add(new MySqlParameter("ds_cpf_cnpj", dto.ds_cpf_cnpj));
            parms.Add(new MySqlParameter("ds_rg", dto.ds_rg));
            parms.Add(new MySqlParameter("ds_cep", dto.ds_cep));
            parms.Add(new MySqlParameter("ds_numero_casa", dto.ds_numero_casa));
            parms.Add(new MySqlParameter("ds_complemento", dto.ds_complemento));

            Database db = new Database();
            return db.ExecuteInsertScriptWithPk(script, parms);
        }

        //public int AlterarCliente(ClienteDTO dto)
        //{
        //    string script = @"UPDATE tb_Cliente SET nm_cliente = @nm_cliente,
        //                                              ds_Email = @ds_Email, 
        //                                              tp_pessoa = @tp_pessoa, 
        //                                       dt_nascimento = @dt_nascimento, 
        //                                           ds_tel_cel = @ds_tel_cel, 
        //                                               ds_cpf_cnpj = @ds_cpf_cnpj, 
        //                                      ds_rg = @ds_rg, 
        //                                         ds_cep = @ds_cep, 
        //                                        ds_numero_casa = @ds_numero_casa, 
        //                                         ds_complemento = @ds_complemento, 

        //                                    WHERE ID_Cliente = @ID_Cliente";

        //    List<MySqlParameter> parms = new List<MySqlParameter>();
        //    parms.Add(new MySqlParameter("ID_Cliente", dto.ID_Cliente));
        //    parms.Add(new MySqlParameter("nm_cliente", dto.nm_cliente));
        //    parms.Add(new MySqlParameter("ds_cpf_cnpj", dto.ds_cpf_cnpj));
        //    parms.Add(new MySqlParameter("ds_Email", dto.ds_Email));
        //    parms.Add(new MySqlParameter("tp_pessoa", dto.tp_pessoa));
        //    parms.Add(new MySqlParameter("dt_nascimento", dto.dt_nascimento));
        //    parms.Add(new MySqlParameter("ds_tel_cel", dto.ds_tel_cel));
        //    parms.Add(new MySqlParameter("ds_rg", dto.ds_rg));
        //    parms.Add(new MySqlParameter("ds_cep", dto.ds_cep));
        //    parms.Add(new MySqlParameter("ds_numero_casa", dto.ds_numero_casa));
        //    parms.Add(new MySqlParameter("ds_complemento", dto.ds_complemento));

        //    Database db = new Database();
        //    return db.ExecuteInsertScriptWithPk(script, parms);
        //}

        public int RemoverCliente(int idCliente)
        {
            string script = "DELETE FROM tb_Cliente WHERE ID_Cliente = @ID_Cliente";
            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("ID_Cliente", idCliente));

            Database db = new Database();
            return db.ExecuteInsertScriptWithPk(script, parms);
        }

        public List<ClienteDTO> Listar()
        {
            string script = @"SELECT * FROM tb_Cliente";

            List<MySqlParameter> parms = new List<MySqlParameter>();

            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            List<ClienteDTO> lista = new List<ClienteDTO>();
            while (reader.Read())
            {
                ClienteDTO dto = new ClienteDTO();
                dto.ID_Cliente = reader.GetInt32("ID_Cliente");
                dto.nm_cliente = reader.GetString("nm_cliente");
                dto.ds_Email = reader.GetString("ds_Email");
                dto.dt_nascimento = reader.GetDateTime("dt_nascimento");
                dto.tp_pessoa = reader.GetString("tp_pessoa");
                dto.ds_cpf_cnpj = reader.GetString("ds_cpf_cnpj");
                dto.ds_rg = reader.GetString("ds_rg");
                dto.ds_tel_cel = reader.GetInt32("ds_tel_cel");
                dto.ds_cep = reader.GetString("ds_cep");
                dto.ds_numero_casa = reader.GetInt32("ds_numero_casa");
                dto.ds_complemento = reader.GetString("ds_complemento");

                lista.Add(dto);
            }
            reader.Close();

            return lista;
        }


        public List<ClienteDTO> ConsultarPorNome(string nome)
        {
            string script = @"SELECT * FROM tb_Cliente WHERE nm_cliente LIKE @nm_cliente";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("nm_cliente", "%" + nome + "%"));

            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            List<ClienteDTO> lista = new List<ClienteDTO>();
            while (reader.Read())
            {
                ClienteDTO dto = new ClienteDTO();
                dto.ID_Cliente = reader.GetInt32("ID_Cliente");
                dto.nm_cliente = reader.GetString("nm_cliente");
                dto.dt_nascimento = reader.GetDateTime("dt_nascimento");
                dto.ds_cpf_cnpj = reader.GetString("ds_cpf_cnpj");
                dto.ds_rg = reader.GetString("ds_rg");
                dto.ds_tel_cel = reader.GetInt32("ds_tel_cel");  

                lista.Add(dto);
            }
            reader.Close();

            return lista;
        }

        public List<ClienteDTO> Consultar()
        {
            string script = @"SELECT * FROM tb_Cliente";

            List<MySqlParameter> parms = new List<MySqlParameter>();

            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            List<ClienteDTO> lista = new List<ClienteDTO>();
            while (reader.Read())
            {
                ClienteDTO dto = new ClienteDTO();
                dto.ID_Cliente = reader.GetInt32("ID_Cliente");
                dto.nm_cliente = reader.GetString("nm_cliente");
                dto.dt_nascimento = reader.GetDateTime("dt_nascimento");
                dto.ds_cpf_cnpj = reader.GetString("ds_cpf_cnpj");
                dto.ds_rg = reader.GetString("ds_rg");
                dto.ds_tel_cel = reader.GetInt32("ds_tel_cel");

                lista.Add(dto);
            }
            reader.Close();

            return lista;
        }


        public ClienteDTO ConsultarPorCpf(string cpf)
        {
            string script = @"SELECT * FROM tb_Cliente WHERE ds_cpf_cnpj LIKE @ds_cpf_cnpj";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("ds_cpf_cnpj", cpf));

            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            ClienteDTO dto = null;
            if (reader.Read())
            {
                dto = new ClienteDTO();
                dto.ID_Cliente = reader.GetInt32("ID_Cliente");
                dto.nm_cliente = reader.GetString("nm_cliente");
                dto.dt_nascimento = reader.GetDateTime("dt_nascimento");
                dto.ds_cpf_cnpj = reader.GetString("ds_cpf_cnpj");
                dto.ds_rg = reader.GetString("ds_rg");
                dto.ds_tel_cel = reader.GetInt32("ds_tel_cel");
            }
            reader.Close();

            return dto;
        }
  }
}

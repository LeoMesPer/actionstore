﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ActionStore.Programação.EstoqueP
{
   public class EstoqueDTO
    {
        //ID_Estoque, fk_Produto, qtd_produto
        public int ID_Estoque { get; set; }
        public int fk_Produto { get; set; }
        public int qtd_produto { get; set; }
    }
}

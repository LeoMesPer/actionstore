﻿using ActionStore.Base;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ActionStore.Programação.EstoqueP
{
    class EstoqueDatabase
    {
        public int Salvar(EstoqueDTO dto)
        {
            string script = @"INSERT INTO tb_Estoque (ID_Estoque, fk_Produto, qtd_produto) 
                                   VALUES (@ID_Estoque, @fk_Produto, @qtd_produto)";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("ID_Estoque", dto.ID_Estoque));
            parms.Add(new MySqlParameter("fk_Produto", dto.fk_Produto));
            parms.Add(new MySqlParameter("qtd_produto", dto.qtd_produto));

            Database db = new Database();
            return db.ExecuteInsertScriptWithPk(script, parms);
        }

        public void Remover(int id)
        {
            string script = @"DELETE FROM tb_Estoque WHERE ID_Estoque = @ID_Estoque";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("ID_Estoque", id));

            Database db = new Database();
            db.ExecuteInsertScript(script, parms);
        }


        public List<EstoqueDTO> Listar()
        {
            string script = @"SELECT * FROM tb_Estoque";

            List<MySqlParameter> parms = new List<MySqlParameter>();

            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            List<EstoqueDTO> lista = new List<EstoqueDTO>();
            while (reader.Read())
            {
                EstoqueDTO dto = new EstoqueDTO();
                dto.ID_Estoque = reader.GetInt32("ID_Estoque");
                dto.fk_Produto = reader.GetInt32("fk_Produto");
                dto.qtd_produto = reader.GetInt32("qtd_produto");
                lista.Add(dto);
            }
            reader.Close();

            return lista;
        }


        public List<EstoqueDTO> Consultar()
        {
            string script = @"SELECT * FROM tb_Estoque";

            List<MySqlParameter> parms = new List<MySqlParameter>();

            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            List<EstoqueDTO> lista = new List<EstoqueDTO>();
            while (reader.Read())
            {
                EstoqueDTO dto = new EstoqueDTO();
                dto.ID_Estoque = reader.GetInt32("ID_Estoque");
                dto.fk_Produto = reader.GetInt32("fk_Produto");
                dto.qtd_produto = reader.GetInt32("qtd_produto");
        
                lista.Add(dto);
            }
            reader.Close();

            return lista;
        }

      

    }
}

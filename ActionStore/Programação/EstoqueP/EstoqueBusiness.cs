﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ActionStore.Programação.EstoqueP
{
    class EstoqueBusiness
    {
        public int Salvar(EstoqueDTO dto)
        {
            if (dto.qtd_produto == 0)
            {
                throw new ArgumentException("Quantidade é obrigatório.");
            }
            if (dto.fk_Produto == 0)
            {
                throw new ArgumentException("Produto é obrigatório.");
            }

            //EstoqueDTO cat = this.Consultar();
            //if (cat != null)
            //{
            //    throw new ArgumentException("Categoria já existe no sistema.");
            //}

            EstoqueDatabase db = new EstoqueDatabase();
            return db.Salvar(dto);
        }


        public void Remover(int id)
        {
            EstoqueDatabase db = new EstoqueDatabase();
            db.Remover(id);
        }



        public List<EstoqueDTO> Consultar()
        {
            EstoqueDatabase db = new EstoqueDatabase();
            return db.Consultar();
        }


        public List<EstoqueDTO> Listar()
        {
            EstoqueDatabase db = new EstoqueDatabase();
            return db.Listar();
        }

    }
}


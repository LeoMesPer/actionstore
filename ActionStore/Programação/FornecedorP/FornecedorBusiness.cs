﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ActionStore.Programação.FornecedorP
{
    class FornecedorBusiness
    {
        public int Salvar(FornecedorDTO dto)
        {
            if (dto.nm_fornecedor == string.Empty)
            {
                throw new ArgumentException("Nome é obrigatório.");
            }

            FornecedorDTO cat = this.ConsultarPorCnpj(Convert.ToString(dto.ds_cnpj));
            if (cat != null)
            {
                throw new ArgumentException("CNPJ já existe no sistema.");
            }

            FornecedorDatabase db = new FornecedorDatabase();
            return db.Salvar(dto);
        }


        public void Remover(int id)
        {
            FornecedorDatabase db = new FornecedorDatabase();
            db.Remover(id);
        }


        public FornecedorDTO ConsultarPorCnpj(string cnpj)
        {
            FornecedorDatabase db = new FornecedorDatabase();
            return db.ConsultarPorCnpj(cnpj);
        }


        public List<FornecedorDTO> Consultar()
        {
            FornecedorDatabase db = new FornecedorDatabase();
            return db.Consultar();
        }


        public List<FornecedorDTO> Listar()
        {
            FornecedorDatabase db = new FornecedorDatabase();
            return db.Listar();
        }

    }
}

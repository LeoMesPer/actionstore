﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ActionStore.Programação.FornecedorP
{
    public class FornecedorDTO
    {
        //ID_Fornecedor, ds_email, nm_fornecedor, ds_tel_cel, ds_cnpj, dt_entrega, vl_preco, ds_cep, ds_numero_empresa, ds_complemento
        public int ID_Fornecedor { get; set; }
        public string ds_email { get; set; }
        public string nm_fornecedor { get; set; }
        public string ds_tel_cel { get; set; }
        public int ds_cnpj { get; set; }
        public DateTime dt_entrega { get; set; }
        public decimal vl_preco { get; set; }
        public string ds_cep { get; set; }
        public int ds_numero_empresa { get; set; }
        public string ds_complemento { get; set; }
    }
}

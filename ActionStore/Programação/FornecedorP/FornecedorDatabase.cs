﻿using ActionStore.Base;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ActionStore.Programação.FornecedorP
{
    class FornecedorDatabase
    {
        public int Salvar(FornecedorDTO dto)
        {
            string script = @"INSERT INTO tb_Fornecedor (ID_Fornecedor, ds_email, nm_fornecedor, ds_tel_cel, ds_cnpj, dt_entrega, vl_preco, ds_cep, ds_numero_empresa, ds_complemento) 
                                   VALUES (@ID_Fornecedor, @ds_email, @nm_fornecedor, @ds_tel_cel, @ds_cnpj, @dt_entrega, @vl_preco, @ds_cep, @ds_numero_empresa, @ds_complemento)";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("ID_Fornecedor", dto.ID_Fornecedor));
            parms.Add(new MySqlParameter("ds_email", dto.ds_email));
            parms.Add(new MySqlParameter("nm_fornecedor", dto.nm_fornecedor));
            parms.Add(new MySqlParameter("ds_tel_cel", dto.ds_tel_cel));
            parms.Add(new MySqlParameter("ds_cnpj", dto.ds_cnpj));
            parms.Add(new MySqlParameter("dt_entrega", dto.dt_entrega));
            parms.Add(new MySqlParameter("vl_preco", dto.vl_preco));
            parms.Add(new MySqlParameter("ds_cep", dto.ds_cep));
            parms.Add(new MySqlParameter("ds_complemento", dto.ds_complemento));
            parms.Add(new MySqlParameter("ds_numero_empresa", dto.ds_numero_empresa));

            Database db = new Database();
            return db.ExecuteInsertScriptWithPk(script, parms);
        }

        public void Remover(int id)
        {
            string script = @"DELETE FROM tb_Fornecedor WHERE ID_Fornecedor = @ID_Fornecedor";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("ID_Fornecedor", id));

            Database db = new Database();
            db.ExecuteInsertScript(script, parms);
        }


        public List<FornecedorDTO> Listar()
        {
            string script = @"SELECT * FROM tb_Fornecedor";

            List<MySqlParameter> parms = new List<MySqlParameter>();

            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            List<FornecedorDTO> lista = new List<FornecedorDTO>();
            while (reader.Read())
            {
                FornecedorDTO dto = new FornecedorDTO();
                dto.ID_Fornecedor = reader.GetInt32("ID_Fornecedor");
                dto.nm_fornecedor = reader.GetString("nm_fornecedor");
                dto.ds_cnpj = reader.GetInt32("ds_cnpj");
                dto.ds_cep = reader.GetString("ds_cep");
                dto.dt_entrega = reader.GetDateTime("dt_entrega");
                dto.vl_preco = reader.GetDecimal("vl_preco");

                lista.Add(dto);
            }
            reader.Close();

            return lista;
        }


        public FornecedorDTO ConsultarPorCnpj(string cnpj)
        {
            string script = @"SELECT * FROM tb_Fornecedor WHERE ds_cnpj = @ds_cnpj";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("ds_cnpj", cnpj));

            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            FornecedorDTO dto = null;
            while (reader.Read())
            {
                dto = new FornecedorDTO();
                dto.ID_Fornecedor = reader.GetInt32("ID_Fornecedor");
                dto.ds_cnpj = reader.GetInt32("ds_cnpj");
            }
            reader.Close();

            return dto;
        }
        public List<FornecedorDTO> Consultar()
        {
            string script = @"SELECT * FROM tb_Fornecedor";

            List<MySqlParameter> parms = new List<MySqlParameter>();

            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            List<FornecedorDTO> lista = new List<FornecedorDTO>();
            while (reader.Read())
            {
                FornecedorDTO dto = new FornecedorDTO();
                dto.ID_Fornecedor = reader.GetInt32("ID_Fornecedor");
                dto.nm_fornecedor = reader.GetString("nm_fornecedor");
                dto.ds_cnpj = reader.GetInt32("ds_cnpj");
                dto.ds_cep = reader.GetString("ds_cep");
                dto.dt_entrega = reader.GetDateTime("dt_entrega");
                dto.vl_preco = reader.GetDecimal("vl_preco");

                lista.Add(dto);
            }
            reader.Close();

            return lista;
        }

    }
}

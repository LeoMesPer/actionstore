﻿using ActionStore.Base;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ActionStore.Programação.PedidoP
{
    class PedidoDatabase
    {
        public int Salvar(PedidoDTO dto)
        {
            string script = @"INSERT INTO tb_Pedido (ID_Pedido, fk_Estoque, fk_Produto, fk_Cliente, vl_qtd_pedido, dt_pedido, vl_total) 
                                   VALUES (@ID_Pedido, @fk_Estoque, @fk_Produto, @fk_Cliente, @vl_qtd_pedido, @dt_pedido, @vl_total)";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("ID_Pedido", dto.ID_Pedido));
            parms.Add(new MySqlParameter("fk_Estoque", dto.fk_Estoque));
            parms.Add(new MySqlParameter("fk_Produto", dto.fk_Produto));
            parms.Add(new MySqlParameter("fk_Cliente", dto.fk_Cliente));
            parms.Add(new MySqlParameter("vl_qtd_pedido", dto.vl_qtd_pedido));
            parms.Add(new MySqlParameter("dt_pedido", dto.dt_pedido));
            parms.Add(new MySqlParameter("vl_total", dto.vl_total));

            Database db = new Database();
            return db.ExecuteInsertScriptWithPk(script, parms);
        }

        public void Remover(int id)
        {
            string script = @"DELETE FROM tb_Pedido WHERE ID_Pedido = @ID_Pedido";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("ID_Pedido", id));

            Database db = new Database();
            db.ExecuteInsertScript(script, parms);
        }


        public List<PedidoDTO> Listar()
        {
            string script = @"SELECT * FROM tb_Pedido";

            List<MySqlParameter> parms = new List<MySqlParameter>();

            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            List<PedidoDTO> lista = new List<PedidoDTO>();
            while (reader.Read())
            {
                PedidoDTO dto = new PedidoDTO();
                dto.ID_Pedido = reader.GetInt32("ID_Pedido");
                dto.fk_Cliente = reader.GetInt32("fk_Cliente");
                dto.fk_Produto = reader.GetInt32("fk_Produto");
                dto.vl_qtd_pedido = reader.GetInt32("vl_qtd_pedido");
                dto.dt_pedido = reader.GetDateTime("dt_pedido");
                dto.vl_total = reader.GetDecimal("vl_total");

                lista.Add(dto);
            }
            reader.Close();

            return lista;
        }


        public List<PedidoDTO> Consultar()
        {
            string script = @"SELECT * FROM tb_Pedido ";

            List<MySqlParameter> parms = new List<MySqlParameter>();

            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            List<PedidoDTO> lista = new List<PedidoDTO>();
            while (reader.Read())
            {
                PedidoDTO dto = new PedidoDTO();
                dto.ID_Pedido = reader.GetInt32("ID_Pedido");
                dto.fk_Cliente = reader.GetInt32("fk_Cliente");
                dto.fk_Produto = reader.GetInt32("fk_Produto");
                dto.vl_qtd_pedido = reader.GetInt32("vl_qtd_pedido");
                dto.dt_pedido = reader.GetDateTime("dt_pedido");
                dto.vl_total = reader.GetDecimal("vl_total");

                lista.Add(dto);
            }
            reader.Close();

            return lista;
        }

        //public PedidoDTO ConsultarPorNome(string nome)
        //{
        //    string script = @"SELECT * FROM tb_Pedido WHERE fk_Cliente = @fk_Cliente";

        //    List<MySqlParameter> parms = new List<MySqlParameter>();
        //    parms.Add(new MySqlParameter("fk_Cliente", nome));

        //    Database db = new Database();
        //    MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

        //    PedidoDTO dto = null;
        //    while (reader.Read())
        //    {
        //        dto = new PedidoDTO();
        //        dto.ID_Pedido = reader.GetInt32("ID_Pedido");
        //        dto.fk_Cliente = reader.GetInt32("fk_Cliente");
        //    }
        //    reader.Close();

        //    return dto;
        //}

    }
}

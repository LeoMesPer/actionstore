﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ActionStore.Programação.PedidoP
{
    class PedidoBusiness
    {
        public int Salvar(PedidoDTO dto)
        {
            if (dto.fk_Cliente == 0)
            {
                throw new ArgumentException("Cliente é obrigatório.");
            }

            //PedidoDTO cat = this.ConsultarPorNome(Convert.ToString(dto.fk_Cliente));
            //if (cat != null)
            //{
            //    throw new ArgumentException("Categoria já existe no sistema.");
            //}

            EstoqueP.EstoqueBusiness estoqueB = new EstoqueP.EstoqueBusiness();
            

            PedidoDatabase db = new PedidoDatabase();
            return db.Salvar(dto);
        }


        public void Remover(int id)
        {
            PedidoDatabase db = new PedidoDatabase();
            db.Remover(id);
        }


        //public PedidoDTO ConsultarPorNome(string nome)
        //{
        //    PedidoDatabase db = new PedidoDatabase();
        //    return db.ConsultarPorNome(nome);
        //}


        public List<PedidoDTO> Consultar()
        {
            PedidoDatabase db = new PedidoDatabase();
            return db.Consultar();
        }


        public List<PedidoDTO> Listar()
        {
            PedidoDatabase db = new PedidoDatabase();
            return db.Listar();
        }
    }
}

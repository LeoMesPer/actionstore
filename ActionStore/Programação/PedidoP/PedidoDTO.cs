﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ActionStore.Programação.PedidoP
{
    public class PedidoDTO
    {
        //ID_Pedido, fk_Estoque, fk_Produto, fk_Cliente, vl_qtd_pedido
        public int ID_Pedido { get; set; }
        public int fk_Estoque { get; set; }
        public int fk_Produto { get; set; }
        public int fk_Cliente { get; set; }
        public int vl_qtd_pedido { get; set; }
        public DateTime dt_pedido { get; set; }
        public decimal vl_total { get; set; }
    }
}

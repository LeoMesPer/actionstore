﻿using ActionStore.Cliente;
using ActionStore.Produto;
using ActionStore.Telas.Estoque;
using ActionStore.Telas.Fluxo_de_Caixa;
using ActionStore.Telas.Fornecedor;
using ActionStore.Telas.Pedido;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ActionStore
{
    public partial class frmMenu : Form
    {
        public frmMenu()
        {
            InitializeComponent();
        }

        private void X_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void cadastrarClienteToolStripMenuItem_Click(object sender, EventArgs e)
        {
            {
                frmCadastrar frm = new frmCadastrar();
                frm.Show();
                Hide();
            }
        }

        private void alterarClienteToolStripMenuItem_Click(object sender, EventArgs e)
        {
        //    {
        //        frmAlterarC frm = new frmAlterarC();
        //        frm.Show();
        //        Hide();
        //    }
        }

        private void consultarClienteToolStripMenuItem_Click(object sender, EventArgs e)
        {
            {
                frmConsultarC frm = new frmConsultarC();
                frm.Show();
                Hide();
            }
        }

        private void cadastrarEstoqueToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //{
            //    frmEstoque frm = new frmEstoque();
            //    frm.Show();
            //    Hide();
            //}
        }

        private void alterarEstoqueToolStripMenuItem_Click(object sender, EventArgs e)
        {
            {
                //frmAlterarE frm = new frmAlterarE();
                //frm.Show();
                //Hide();
            }
        }

        private void consultarEstoqueToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //{
            //    frmConsultarE frm = new frmConsultarE();
            //    frm.Show();
            //    Hide();
            //}
        }

        private void fluxoDeCaixaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //{
            //    frmFluxo frm = new frmFluxo();
            //    frm.Show();
            //    Hide();
            //}
        }

        private void cadastrarFornecedorToolStripMenuItem_Click(object sender, EventArgs e)
        {
            {
                frmCadastrarF frm = new frmCadastrarF();
                frm.Show();
                Hide();
            }
        }

        private void alterarFornecedorToolStripMenuItem_Click(object sender, EventArgs e)
        {
        //    {
        //        frmAlterarF frm = new frmAlterarF();
        //        frm.Show();
        //        Hide();
        //    }
        }

        private void consultarFornecedorToolStripMenuItem_Click(object sender, EventArgs e)
        {
            {
                frmConsultarF frm = new frmConsultarF();
                frm.Show();
                Hide();
            }
        }

        private void cadastrarPedidoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            {
                frmCadastrarPD frm = new frmCadastrarPD();
                frm.Show();
                Hide();
            }
        }

        private void alterarPedidoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            {
                //frmAlterarPD frm = new frmAlterarPD();
                //frm.Show();
                //Hide();
            }
        }

        private void consultarPedidoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            {
                ConsultarPD frm = new ConsultarPD();
                frm.Show();
                Hide();
            }
        }

        private void cadastrarProdutoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            {
                frmCadastrarPT frm = new frmCadastrarPT();
                frm.Show();
                Hide();
            }
        }

        private void alterarProdutoToolStripMenuItem_Click(object sender, EventArgs e)
        {
        //    {
        //        frmAlterarPT frm = new frmAlterarPT();
        //        frm.Show();
        //        Hide();
        //    }
        }

        private void consultarProdutoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            {
                frmConsultarPT frm = new frmConsultarPT();
                frm.Show();
                Hide();
            }
        }

        private void menuToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void estoqueToolStripMenuItem_Click(object sender, EventArgs e)
        {
            {
                frmEstoque frm = new frmEstoque();
                frm.Show();
                Hide();
            }
        }

        private void fornecedorToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }
    }
}
